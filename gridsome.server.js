/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const axios = require("axios");
const requireMaybe = require("require-maybe");
const slugify = require("@sindresorhus/slugify");

const elaborator = require("./src/js/elaborateData.js");
const tests = require("./src/js/testData.js");

const installerData = requireMaybe("./data/installerData.json") || [];

// Display a warning if installer data wasn't downloaded
if (!installerData.length) {
  console.log(
    "\x1b[33m%s\x1b[0m",
    "Installer data unavailable at build time.\n",
    "You should consider running one of the following commands instead:\n",
    "npm run develop : Fetch data and run in development mode\n",
    "npm run build : Fetch data and build site for production mode\n",
    "npm run get-data : Only fetch data"
  );
}

module.exports = function (api) {
  // Set installer redirects
  api.loadSource(async (actions) => {
    // Generated redirects are stored in the NetlifyRedirects collection for the redirects plugin
    const netlifyRedirects = actions.addCollection("NetlifyRedirects");

    // Get links from Github
    async function getAssetUrl(packageType) {
      try {
        const results = await axios.get(
          "https://api.github.com/repos/ubports/ubports-installer/releases/latest"
        );
        return results.data.assets.find((asset) =>
          asset.name.toLowerCase().endsWith(packageType.toLowerCase())
        ).browser_download_url;
      } catch (error) {
        console.log(error);
        return "/installer";
      }
    }

    // Save links to NetlifyRedirects collection
    for (let el of ["exe", "deb", "dmg", "appimage"]) {
      netlifyRedirects.addNode({
        from: "/installer/" + el,
        to: await getAssetUrl(el),
        status: 302
      });
    }

    // The link for the snap package is not stored at Github ( direct link is provided )
    netlifyRedirects.addNode({
      from: "/installer/snap",
      to: "https://snapcraft.io/ubports-installer",
      status: 302
    });
  });

  api.onCreateNode((options) => {
    if (options.internal.typeName === "Device" && !options.elaborated) {
      elaborator.importData(options, installerData);
      tests.runBefore(options);
      elaborator.run(options);
      tests.runAfter(options);
      elaborator.clean(options);
      options.elaborated = true;
    }
  });

  // Create aliases for devices
  api.createPages(async ({ graphql, createPage }) => {
    const { data } = await graphql(
      "{ allDevice { edges { node { id codename aliases path } } } }"
    );

    data.allDevice.edges.forEach(({ node }) => {
      for (let alias of node.aliases) {
        createPage({
          path: "/device/" + slugify(alias, { decamelize: false }),
          component: "./src/templates/Device.vue",
          context: {
            id: node.id // provide the id of the primary device name to query data
          }
        });
      }
    });
  });
};
